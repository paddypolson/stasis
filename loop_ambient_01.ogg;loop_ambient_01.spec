# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['loop_ambient_01.ogg;loop_ambient_01.ogg', 'main.py'],
             pathex=['C:\\Users\\paddy\\OneDrive\\gmtk\\test'],
             binaries=[],
             datas=[('beep_03.ogg', 'beep_03.ogg')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='loop_ambient_01.ogg;loop_ambient_01',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True )
