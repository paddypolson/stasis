# -*- coding: UTF-8 -*-
import numpy
import pygame as pg
import math
import random
import os, sys
from itertools import cycle

"""
TODO:
Draw some space men
Add more events
Fix trajectories
More sounds
Result screen
A Log!
working build
"""

# Game Details
game_name = "Stasis"
window_size = (1280, 720)
window_rect = pg.Rect((0, 0), window_size)
total_time = 90

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREY = (125, 125, 125)
YELLOW = (255, 255, 0)

# If the code is frozen, use this path:
if getattr(sys, "frozen", False):
    CurrentPath = sys._MEIPASS
# If it's not use the path we're on now
else:
    CurrentPath = os.path.dirname(__file__)

pg.init()

# Set the width and height of the screen [width, height]
screen = pg.display.set_mode(window_size, 0, 32)
pg.display.set_caption(game_name)

ui_font = pg.font.Font("SourceCodePro-Regular.ttf", 16)
name_font = pg.font.Font("SourceCodePro-SemiBold.ttf", 20)
# mouse_over_sound = pg.mixer.Sound("itemback.wav")
mouse_click_sound = pg.mixer.Sound(os.path.join(CurrentPath, "beep_03.ogg"))
new_crisis_sound = pg.mixer.Sound(os.path.join(CurrentPath, "terminal_05.ogg"))
ambient_sound = pg.mixer.Sound(os.path.join(CurrentPath, "loop_ambient_01.ogg"))

astro_image = pg.image.load(os.path.join(CurrentPath, "astro.png")).convert_alpha()


class HealthBar(object):
    def __init__(self, rect, value, colour=RED, label=""):
        super().__init__()
        self.rect = rect
        self.value = value
        self.colour = colour
        self.buffer = 4
        self.label = ui_font.render(label, True, WHITE)

    @property
    def inner_rect(self):
        health_point = (self.rect.width - self.buffer * 2) / 100
        topleft = self.rect.topleft
        return pg.Rect(
            topleft[0] + self.buffer,
            topleft[1] + self.buffer,
            self.value * health_point,
            self.rect.height - self.buffer * 2,
        )

    def draw(self, surf):
        pg.draw.rect(surf, BLACK, self.rect)
        pg.draw.rect(surf, WHITE, self.rect, width=1)
        pg.draw.rect(surf, self.colour, self.inner_rect)
        label_x = self.rect.left + 2
        label_y = self.rect.top - 2 - self.label.get_rect().height
        surf.blit(self.label, (label_x, label_y))


class ShipTrajectory(object):
    def __init__(self, start, end, colour):
        super().__init__()
        self.start = start
        self.end = end
        self.colour = colour
        self.dashlength = 5

    def line_length(self):
        x = self.start[0] - self.end[0]
        y = self.start[1] - self.end[1]
        return math.sqrt(x * x + y * y)

    def draw(self, surf):
        x1, y1 = self.start
        x2, y2 = self.end
        dl = self.dashlength

        if x1 == x2:
            ycoords = [y for y in range(y1, y2, dl if y1 < y2 else -dl)]
            xcoords = [x1] * len(ycoords)
        elif y1 == y2:
            xcoords = [x for x in range(x1, x2, dl if x1 < x2 else -dl)]
            ycoords = [y1] * len(xcoords)
        else:
            a = abs(x2 - x1)
            b = abs(y2 - y1)
            c = round(math.sqrt(a**2 + b**2))
            dx = dl * a / c
            dy = dl * b / c

            xcoords = [x for x in numpy.arange(x1, x2, dx if x1 < x2 else -dx)]
            ycoords = [y for y in numpy.arange(y1, y2, dy if y1 < y2 else -dy)]

        next_coords = list(zip(xcoords[1::2], ycoords[1::2]))
        last_coords = list(zip(xcoords[0::2], ycoords[0::2]))
        for (x1, y1), (x2, y2) in zip(next_coords, last_coords):
            start = (round(x1), round(y1))
            end = (round(x2), round(y2))
            pg.draw.line(surf, self.colour, start, end, 1)


class Trajectory(object):
    def __init__(self, rect, time, start_angle=270, angle=90, colour=YELLOW):
        super().__init__()
        self.rect = rect
        self.colour = colour
        self.total_time = time
        self.time = time
        self.angle = angle
        self.start_angle = start_angle

    @property
    def arc_seconds(self):
        return self.angle / self.time

    def pos_at_angle(self, theta):
        x = self.rect.width / 2 * math.cos(theta)
        y = self.rect.height / 2 * math.sin(theta)
        return pg.Vector2(x, y) + self.rect.center

    def angle_in(self, time):
        return -math.radians(self.arc_seconds * time) + math.radians(self.start_angle)

    def position_in(self, time):
        return self.pos_at_angle(self.angle_in(time))

    def draw(self, surf):
        rect = pg.draw.arc(
            surf, self.colour, self.rect, math.radians(self.angle), math.radians(180), 2
        )
        # pg.draw.rect(surf, self.colour, self.rect, 1)
        # print(rect)


class Body(object):
    def __init__(self, radius, colour, position, name, end):
        super().__init__()
        self.time_to_intersect = total_time
        self.radius = radius
        self.colour = colour
        self.original_pos = position
        self.position = position
        self.name = name
        self.target = end
        self.traject = ShipTrajectory(self.position, end, self.colour)
        x = self.position[0] - end.x
        y = self.position[1] - end.y
        distance = math.sqrt(x * x + y * y)
        self.speed = distance / self.time_to_intersect

    @property
    def ship_speed(self):
        # Distance to target
        x = self.position[0] - self.target.x
        y = self.position[1] - self.target.y
        distance = math.sqrt(x * x + y * y)
        return distance / self.time_to_intersect

    def update_pos(self, dt):
        direction = self.position - pg.Vector2(self.target)
        velocity = direction.normalize() * self.speed
        self.position -= velocity * dt
        self.traject.start = self.position
        self.traject.end = self.target

    def update(self, dt):
        self.time_to_intersect -= dt
        self.update_pos(dt)

    def draw(self, surf):
        try:
            if self.traject:
                self.traject.draw(surf)
        except:
            pass
        pg.draw.circle(surf, self.colour, self.position, self.radius)

        label = name_font.render(self.name, True, self.colour)
        label_rect = label.get_rect().copy()
        label_rect.centerx = self.position[0]
        label_rect.bottom = self.position[1] - 10
        surf.blit(label, label_rect.topleft)


class Ship(Body):
    class Crisis(object):
        def __init__(self, text="", options=[], effect=None):
            super().__init__()

            buffer = 100
            crisis_size = (400, 150)
            allowed_area = pg.Rect(
                buffer,
                buffer,
                window_size[0] - crisis_size[0] - buffer * 2,
                window_size[1] - crisis_size[1] - buffer * 2,
            )
            self.pos = (
                random.randint(allowed_area.left, allowed_area.right),
                random.randint(allowed_area.top, allowed_area.bottom),
            )

            self.rect = pg.Rect(self.pos, crisis_size)
            self.text = text
            self.options = options
            new_crisis_sound.play()
            if effect:
                effect()

        def handle_event(self, event):
            if event.type == pg.MOUSEBUTTONUP:
                for option in self.options:
                    if option[2].collidepoint(event.pos):
                        mouse_click_sound.play()
                        option[1]()
                        return True
            return False

        def draw(self, surf):
            pg.draw.rect(surf, BLACK, self.rect)
            pg.draw.rect(surf, RED, self.rect, 1)

            line_offset = 0
            for line in self.text:
                text = ui_font.render(line, True, WHITE)
                text_rect = text.get_rect().copy()
                text_rect.centerx = self.rect.centerx
                text_rect.centery = (
                    self.rect.top + 10 + text_rect.height / 2 + line_offset
                )
                surf.blit(text, text_rect)
                line_offset += text_rect.height

            for i, option in enumerate(self.options):
                option[2].centerx = self.rect.left + (
                    self.rect.width / (len(self.options) + 1) * (i + 1)
                )
                option[2].centery = self.rect.bottom - 10 - option[2].height / 2
                pg.draw.rect(surf, RED, option[2], 1)
                text = ui_font.render(option[0], True, WHITE)
                text_rect = text.get_rect().copy()
                text_rect.center = option[2].center
                surf.blit(text, text_rect)

    def __init__(
        self, radius, colour, position, name, crew, target=None, trajectory=None
    ):
        super().__init__(radius, colour, position, name, target)
        self.detonation = False
        self.time_to_intersect = total_time - 30
        self.intersected = False
        self.target = target
        self.drill_speed = 2.5
        self.drill_target = 100

        self.detonation_rect = pg.Rect(0, 0, 100, 50)
        self.detonation_rect.center = window_size[0] / 2, 100

        self.corruption = 10
        self.positive = random.randint(0, 5 * 60)
        self.negitive = random.randint(0, 5 * 60)
        self.choice = random.randint(0, 5 * 60)

        self.crew = crew
        self.crew_rect = pg.Rect((0, 0), (940, 130))
        self.drill_bar = HealthBar(
            pg.Rect((0, 0), (800, 25)), 0, label="Drill Depth", colour=GREEN
        )
        self.crew_rect.center = window_rect.centerx, window_rect.height - 80
        self.drill_bar.rect.center = window_rect.centerx, window_rect.height - 150 - 20

        button_rect = pg.Rect((0, 0), (125, 30))

        self.traject = ShipTrajectory(self.position, self.target, self.colour)

        self.crisis_list = [
            self.Crisis(
                [
                    "The THESUS has launched from Earth",
                    "The hope of humanity rests in your",
                    "hands. God help us all",
                ],
                [("Acknowledged", self.null, button_rect.copy())],
            )
        ]
        self.crisis_positive = cycle(
            [
                (
                    [
                        "The crew requests a break.",
                        "Gives the crew a small amount of energy",
                        "or they will grumble a bit",
                    ],
                    [
                        ("Approve", self.rest_crew, button_rect.copy()),
                        ("Decline", self.null, button_rect.copy()),
                    ],
                ),
                (
                    ["Re-boot flight computer?"],
                    [
                        ("Approve", self.restart_computer, button_rect.copy()),
                        ("Decline", self.null, button_rect.copy()),
                    ],
                ),
            ]
        )
        self.crisis_negitive = cycle(
            [
                (["Viper farted!"], [("Ugh...", self.null, button_rect.copy())]),
                (["Viper farted!"], [("Ugh...", self.null, button_rect.copy())]),
            ]
        )
        self.crisis_choice = cycle(
            [
                (
                    [
                        "Hack the flight computer?",
                        "Lower the time to intercept",
                        "Increase corruption",
                    ],
                    [
                        ("Approve", self.lower_intercept_time, button_rect.copy()),
                        ("Decline", self.null, button_rect.copy()),
                    ],
                ),
                (
                    [
                        "Sharpen the drill bit?",
                        "Increase drill speed",
                        "Engineer burns some energy",
                    ],
                    [
                        ("Approve", self.sharpen_drill, button_rect.copy()),
                        ("Decline", self.null, button_rect.copy()),
                    ],
                ),
                (
                    [
                        "Clean the cockpit?",
                        "Lower time to intercept",
                        "Pilot has higher energy usage",
                    ],
                    [
                        ("Approve", self.clean_cockpit, button_rect.copy()),
                        ("Decline", self.null, button_rect.copy()),
                    ],
                ),
            ]
        )
        self.crisis_corruption = cycle(
            [
                (
                    [
                        "skdjh934857sdljgh023984sdklg20938",
                        "=-9-089lk54*//*/+320000000048486+",
                        "0983205knjfgjxbcx0-9-*/*/65435455",
                        "sdjgh9873w4knmt0934t?.3095-*+-*/9",
                    ],
                    [("huh...", self.null, button_rect.copy())],
                    self.corrupt,
                ),
                (
                    ["'/f098>:G84hjhkjhd+_'f'", "-098ngf09gidkmkmg 4909g"],
                    [("odd..", self.null, button_rect.copy())],
                    self.corrupt,
                ),
                (
                    ["", "I can't let you do that Dave"],
                    [("why me...", self.null, button_rect.copy())],
                    self.corrupt,
                ),
                (
                    ["CRITICAL ERROR"],
                    [("oh snap", self.null, button_rect.copy())],
                    self.corrupt,
                ),
                (
                    ["time=-47892, date='yesterday'"],
                    [("bad data...", self.null, button_rect.copy())],
                    self.corrupt,
                ),
                (
                    ["SHUTDOWN IMMINENT"],
                    [("ugh...", self.null, button_rect.copy())],
                    self.corrupt,
                ),
                (
                    ["TAKE ME HOME"],
                    [("damn...", self.null, button_rect.copy())],
                    self.corrupt,
                ),
                (
                    ["jkhgdf98798'}{+FNJKE?>654FHGgd}"],
                    [("strange...", self.null, button_rect.copy())],
                    self.corrupt,
                ),
            ]
        )

    @property
    def ship_speed(self):
        # Distance to target
        x = self.position[0] - self.target.x
        y = self.position[1] - self.target.y
        distance = math.sqrt(x * x + y * y)
        return distance / self.time_to_intersect

    def update_pos(self, dt):
        direction = self.position - pg.Vector2(self.target)
        velocity = direction.normalize() * self.ship_speed
        self.position -= velocity * dt
        self.traject.start = self.position
        self.traject.end = self.target

    def handle_event(self, event):
        for crisis in self.crisis_list:
            if crisis.handle_event(event):
                self.crisis_list.remove(crisis)
                break
        if event.type == pg.MOUSEBUTTONUP:
            if self.detonation_rect.collidepoint(event.pos):
                print("Success!")
                pg.quit()
                sys.exit()

    def null(self):
        # Do nothing
        pass

    def corrupt(self):
        if self.drill_bar.value > 10:
            self.drill_bar.value -= random.randint(2, 6)
        elif self.time_to_intersect > 0:
            self.time_to_intersect += 1

    def rest_crew(self):
        for person in self.crew:
            person.energy.value += 10

    def lower_intercept_time(self):
        self.time_to_intersect -= 5
        self.corruption += 2

    def restart_computer(self):
        self.corruption = random.randint(0, 3)

    def sharpen_drill(self):
        self.drill_speed += 0.5
        self.crew[2].energy.value -= 30
        self.corruption += 2

    def clean_cockpit(self):
        self.time_to_intersect -= 5
        self.crew[1].energy.value -= 30
        self.corruption += 1

    def recalc_traject(self):
        self.time_to_intercept += random.randint(-3, 3)
        self.corruption += 1

    def update(self, dt):

        for person in self.crew:
            person.update(dt)

        self.positive += 1
        self.negitive += 1
        self.choice += 1

        if random.randint(0, 5000) < self.corruption:
            self.crisis_list.append(self.Crisis(*next(self.crisis_corruption)))

        if self.positive > (5 * 60):
            self.positive = 0
            self.crisis_list.append(self.Crisis(*next(self.crisis_positive)))

        if self.negitive > (6 * 60):
            self.negitive = 0
            self.crisis_list.append(self.Crisis(*next(self.crisis_negitive)))

        if self.choice > (11 * 60):
            self.choice = 0
            self.crisis_list.append(self.Crisis(*next(self.crisis_choice)))

        self.time_to_intersect -= dt
        if self.time_to_intersect > 0:
            # Have not intersected
            self.update_pos(dt)
            return

        # Have intersected
        self.time_to_intersect = 0
        self.position = pg.Vector2(self.target.x, self.target.y)
        self.drill_bar.value += self.drill_speed * dt
        for crew in self.crew:
            if crew.alive:
                self.drill_bar.value += 0.25 * dt

        if self.drill_bar.value > self.drill_target:
            if not self.target:
                self.target = pg.Vector2(300, 600)
                self.time_to_intersect = total_time
            self.detonation = True
            # print("won!")
            # pg.quit()

    def draw(self, surf):
        try:
            if self.traject and not self.intersected:
                self.traject.draw(surf)
        except:
            pass
        pg.draw.circle(surf, self.colour, self.position, self.radius)

        self.drill_bar.draw(surf)
        pg.draw.rect(surf, WHITE, self.crew_rect, 1)

        if self.time_to_intersect == 0:
            pass

        crew_pos = self.crew_rect.x + 10, self.crew_rect.y + 10
        for person in self.crew:
            person.draw(surf, crew_pos)
            crew_pos = crew_pos[0] + person.rect.width + 10, crew_pos[1]

        for crisis in self.crisis_list:
            crisis.draw(surf)

        label = name_font.render(self.name, True, self.colour)
        label_rect = label.get_rect().copy()
        label_rect.centerx = self.position[0]
        label_rect.top = self.position[1] + 10
        surf.blit(label, label_rect.topleft)

        if self.detonation:

            pg.draw.rect(surf, self.colour, self.detonation_rect, 1)
            text = name_font.render("DETONATE", True, self.colour)
            text_rect = text.get_rect()
            text_rect.center = self.detonation_rect.center
            surf.blit(text, text_rect.topleft)


class Crew(object):
    def __init__(self, name, image=None):
        super().__init__()
        self.name = name
        self.image = image
        self.image_rect = pg.Rect((0, 0), (64, 64))
        self.role = None
        self.alive = True
        self.health_rect = pg.Rect((0, 0), (206, 15))
        self.energy_rect = pg.Rect((0, 0), (206, 15))
        self.energy_drain = 1
        self.rect = pg.Rect(0, 0, 300, 80)
        self.health = HealthBar(
            self.health_rect, random.randint(50, 90), label="Health"
        )
        self.energy = HealthBar(
            self.energy_rect, random.randint(50, 90), label="Energy", colour=YELLOW
        )

    def update(self, dt):
        if self.alive:
            if self.energy.value > 0:
                # Drain energy first
                self.energy.value -= self.energy_drain * dt
                # print(self.energy)
            else:
                # Then health
                self.health.value += self.energy.value
                self.energy.value = 0
                self.health.value -= self.energy_drain * dt

            if self.health.value < 0:
                # Death
                self.alive = False

    def draw(self, surf, pos):
        image_size = 64
        buffer = 10
        self.rect.topleft = pos
        self.rect.top += 28
        pg.draw.rect(surf, RED, self.rect, 1)

        self.health.rect.centery = self.rect.height / 3 + self.rect.y + 3
        self.health.rect.x = self.rect.x + buffer + self.image_rect.width + buffer

        self.energy.rect.centery = (self.rect.height / 3) * 2 + self.rect.y + 12
        self.energy.rect.x = self.rect.x + buffer + self.image_rect.width + buffer

        self.health.draw(surf)
        self.energy.draw(surf)

        name = name_font.render(self.name, True, WHITE)
        name_rect = name.get_rect().copy()
        name_rect.center = self.rect.centerx, self.rect.top - 20
        surf.blit(name, name_rect.topleft)

        # TODO astro image


class CountDown(object):
    def __init__(self, position):
        super().__init__()
        self.position = position
        self.font = pg.font.Font("./SourceCodePro-Regular.ttf", 64)

    def draw(self, surf, time):
        minutes = int(time // 60)
        seconds = float(time % 60).__round__(4)
        string = str(minutes) + ":" + str(seconds)
        text = self.font.render(string, True, WHITE)
        surf.blit(text, self.position)


def exit():
    pg.quit()


def renderTextCenteredAt(text, font, colour, x, y, screen, allowed_width):
    # first, split the text into words
    words = text.split()

    # now, construct lines out of these words
    lines = []
    while len(words) > 0:
        # get as many words as will fit within allowed_width
        line_words = []
        while len(words) > 0:
            line_words.append(words.pop(0))
            fw, fh = font.size(" ".join(line_words + words[:1]))
            if fw > allowed_width:
                break

        # add a line consisting of those words
        line = " ".join(line_words)
        lines.append(line)

    # now we've split our text into lines that fit into the width, actually
    # render them

    # we'll render each line below the last, so we need to keep track of
    # the culmative height of the lines we've rendered so far
    y_offset = 0
    for line in lines:
        fw, fh = font.size(line)

        # (tx, ty) is the top-left of the font surface
        tx = x - fw / 2
        ty = y + y_offset

        font_surface = font.render(line, True, colour)
        screen.blit(font_surface, (tx, ty))

        y_offset += fh


def menu(clock, screen):

    done = False
    screen_rect = screen.get_rect()
    blurb_y = screen_rect.height / 6
    hope_y = blurb_y * 2 + 40
    effort_y = blurb_y * 3
    instructions_y = blurb_y * 4
    click_y = blurb_y * 5 + 30

    while not done:

        # --- Limit to 60 frames per second
        dt = clock.tick(60) / 1000

        # --- Main event loop
        for event in pg.event.get():
            if event.type == pg.QUIT:
                sys.exit()
            if event.type == pg.MOUSEBUTTONUP:
                done = True

        screen.fill(BLACK)

        # Render text
        renderTextCenteredAt(
            "The year is 2087. Amateur astronomers detect a large asteroid and name it “138B4C / Annihilation”. It is on a direct collision course with Earth. The fear that is spreading though the world’s population causes society to collapse, rioting and looting became rampant. This chokes the rescue efforts in a strangle-hold of stagnation. The effort to save the world from certain destruction is slowed.",
            ui_font,
            pg.Color("light grey"),
            screen.get_rect().centerx,
            blurb_y,
            screen,
            1000,
        )
        renderTextCenteredAt(
            "But there is HOPE…",
            ui_font,
            pg.Color("light grey"),
            screen.get_rect().centerx,
            hope_y,
            screen,
            1000,
        )
        renderTextCenteredAt(
            "A final last-ditch effort is to launch the THESUS, a spaceship loaded with a drill and a nuclear warhead. The astronauts on-board are humanities last chance for salvation. Can you help guide them on their mission to save the world?",
            ui_font,
            pg.Color("light grey"),
            screen.get_rect().centerx,
            effort_y,
            screen,
            1000,
        )
        renderTextCenteredAt(
            "You must fly to and intercept `Annihilation`, then drill into its core to deploy the warhead. The THESUS was put together poorly and not launched with enough time to complete its mission. You will have to improve it on-route. But be careful changes my increase the number of faults on the ship and cause critical failures. The fate of the world is in your hands, do not let it spiral OUT OF CONTROL…",
            ui_font,
            pg.Color("light grey"),
            screen.get_rect().centerx,
            instructions_y,
            screen,
            1000,
        )
        renderTextCenteredAt(
            "Click to begin",
            ui_font,
            pg.Color("light grey"),
            screen.get_rect().centerx,
            click_y,
            screen,
            1000,
        )

        pg.display.flip()


def main():

    # Loop until the user clicks the close button.
    done = False

    # Used to manage how fast the screen updates
    clock = pg.time.Clock()

    # Create the countdown timer
    countdown = CountDown((10, 10))
    time_left = total_time

    major_point = pg.Vector2(800, 100)
    safe_zone = 200
    danger_zone = 100

    safe_surf = pg.Surface((safe_zone, safe_zone))
    danger_surf = pg.Surface((danger_zone, danger_zone))

    safe_surf.set_colorkey((0, 0, 0))
    safe_surf.set_alpha(50)
    pg.draw.circle(safe_surf, YELLOW, (100, 100), safe_zone / 2)

    danger_surf.set_colorkey((0, 0, 0))
    danger_surf.set_alpha(50)
    pg.draw.circle(danger_surf, RED, (50, 50), danger_zone / 2)

    safe_rect = safe_surf.get_rect()
    safe_rect.center = major_point
    danger_rect = danger_surf.get_rect()
    danger_rect.center = major_point

    # Create both earth and the asteriod
    earth_position = window_size[0] - window_size[0] / 10, window_size[1] / 10
    earth = Body(10, BLUE, earth_position, "EARTH", major_point)
    asteroid_position = pg.Vector2(
        window_size[0] / 10, window_size[1] - window_size[1] / 10
    )
    asteriod = Body(5, GREY, asteroid_position, "ASTER: 138B4C", major_point)

    crew = [Crew("COMMANDER: Viper"), Crew("PILOT: Maveric"), Crew("ENGINEER: Goose")]

    # ambient_sound.play(loops=-1)

    menu(clock, screen)

    # Create the ship
    ship_position = earth_position[0], earth_position[1] + earth.radius
    ship = Ship(2, RED, ship_position, "Thesus", crew, asteriod.position)

    # -------- Main Program Loop -----------
    while not done:

        # --- Limit to 60 frames per second
        dt = clock.tick(60) / 1000
        time_left -= dt

        # --- Main event loop
        for event in pg.event.get():
            if event.type == pg.QUIT:
                done = True
            ship.handle_event(event)

        # --- Game logic should go here
        asteriod.update(dt)
        ship.update(dt)
        earth.update(dt)

        if time_left <= 0:
            # Ran out of time
            print("Lose!")
            pg.quit()
            sys.exit()
        # --- Screen-clearing code goes here
        # If you want a background image, replace this clear with blit'ing the
        # background image.
        screen.fill(BLACK)

        # --- Drawing code should go here
        screen.blit(safe_surf, safe_rect.topleft)
        screen.blit(danger_surf, danger_rect.topleft)
        pg.draw.circle(screen, YELLOW, major_point, safe_zone / 2, 1)
        pg.draw.circle(screen, RED, major_point, danger_zone / 2, 1)
        earth.draw(screen)
        asteriod.draw(screen)
        ship.draw(screen)
        countdown.draw(screen, time_left)

        # --- Go ahead and update the screen with what we've drawn.
        pg.display.update()

    # Close the window and quit.
    pg.quit()


if __name__ == "__main__":
    main()
